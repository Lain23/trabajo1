﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Disparo3 : MonoBehaviour
{
    public float range = 30f;
    public GameObject effect;
    int CubosDestruidos = 0;
    int CantidadDisparos = 0;
    public AudioClip SonidoDisparo;
    AudioSource Audio;

    public void Start()
    {
        Audio = GetComponent<AudioSource>();
    }


    private void Update()
    {
        //Disparo
        if (Input.GetMouseButtonDown(0))
        {
            Audio.PlayOneShot(SonidoDisparo);
            RaycastHit hit;

            if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out hit, range))
            {
                //Audio.PlayOneShot(SonidoDisparo);
                //Destruccion del cubo
                CantidadDisparos++;
                //Destroy(hit.collider.gameObject);
                //Destroy(hit.collider);

                if (CantidadDisparos == 3)
                {
                    //Efecto de bala
                    GameObject _effect = Instantiate(effect, hit.point, Quaternion.identity);
                    Destroy(_effect, 0.5f);
                    Debug.Log(hit.collider.name);
                    CantidadDisparos = 0;
                    if (hit.collider.gameObject.tag == "Enemy")
                    {
                        CubosDestruidos = CubosDestruidos + 1;
                        Destroy(hit.collider.gameObject);
                    }
                    //Audio.PlayOneShot(SonidoDisparo);
                }
            }
            Deteccion();
        }
    }



    private void Deteccion()
    {
        if (CubosDestruidos == 5)
        {
            Debug.Log("GAME OVER");
            //SceneManager.LoadScene("nivel2");
        }
    }

    public void LoadScene(string SceneName)
    {
        if (CubosDestruidos == 5)
        {
            SceneManager.LoadScene(SceneName);
            Debug.Log("Cargando escena " + SceneName);
        }
    }
    private void scene2()
    {
        if (CubosDestruidos == 5)
        {
            Debug.Log("Cargando escena 2 \n");
            SceneManager.LoadScene(2);
        }
    }

    private void scene3()
    {
        if (CubosDestruidos == 5)
        {
            Debug.Log("Cargando escena 3 \n");
            SceneManager.LoadScene(3);
        }
    }

    private void OnDrawGizmos()
    {
        //Color y distancia de la mira en scene
        Gizmos.color = Color.red;
        Gizmos.DrawRay(Camera.main.transform.position, Camera.main.transform.forward * range);
    }


}
