﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movimiento : MonoBehaviour
{
    public float movementspeed = 20f;

    Vector3 targetPosition;
    Vector3 towardsTarget;

    float wanderRadius = 3f;

    void RecalculateTargetPosition() {
        targetPosition = transform.position + Random.insideUnitSphere * wanderRadius;
    }

    // Start is called before the first frame update
    void Start()
    {
        RecalculateTargetPosition();
        
    }

    // Update is called once per frame
    void Update()
    {
        towardsTarget = targetPosition - transform.position;
        if (towardsTarget.magnitude < 1f)
            RecalculateTargetPosition();

        transform.position += towardsTarget.normalized * movementspeed * Time.deltaTime;

        Debug.DrawLine(transform.position, targetPosition, Color.green);

        
    }
}
