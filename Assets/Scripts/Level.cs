﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Level : MonoBehaviour
{
    public void SiguienteNivel() 
    {
        int EscenaActualIndice = SceneManager.GetActiveScene().buildIndex;
        int SiguienteEscenaIndice = EscenaActualIndice++;
        SceneManager.LoadScene(SiguienteEscenaIndice);
    }
}
