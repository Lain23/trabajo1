﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterController))]
public class PlayerController : MonoBehaviour
{
    [Header ("Referencias")]
    public Camera playerCamera;

    [Header ("General")]
    public float gravityScale = -20f; //gravedad

    [Header ("Movimiento")]
    public float PlayerSpeed = 5f; 
    
    [Header ("Salto")]
    public float Salto = 1.9f;

    [Header("Rotacion")]
    public float rotacion = 10f;


    private float camaraVertical;



    Vector3 moveInput = Vector3.zero;
    Vector3 rotationInput = Vector3.zero;
    CharacterController characterController;

    private void Awake()
    {
        characterController = GetComponent<CharacterController>();
    }

    private void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
    }

    private void Update()
    {
        Move();
        Look();
    }

    private void Move()
    { 

        if (characterController.isGrounded)
        {
            moveInput = new Vector3(Input.GetAxis("Horizontal"), 0f, Input.GetAxis("Vertical"));
            moveInput = transform.TransformDirection(moveInput) * PlayerSpeed;

            if (Input.GetButtonDown("Jump"))
            {
                moveInput.y = Mathf.Sqrt(Salto * -2f * gravityScale);
            }
        }

        moveInput.y += gravityScale * Time.deltaTime; // para que el jugador sea afectado por la gravedad

        characterController.Move(moveInput * Time.deltaTime);

    }

    private void Look()
    {
        rotationInput.x = Input.GetAxis("Mouse X") * rotacion * Time.deltaTime;
        rotationInput.y = Input.GetAxis("Mouse Y") * rotacion * Time.deltaTime;

        transform.Rotate(Vector3.up * rotationInput.x);

        camaraVertical += rotationInput.y;
        camaraVertical = Mathf.Clamp(camaraVertical,-70,70);

        playerCamera.transform.localRotation = Quaternion.Euler(-camaraVertical,0f,0f);
    }





}
