﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawn : MonoBehaviour
{
    public GameObject cubos;
    public float timepodeSpawn = 0.05f, Rangocreacion = 2f, maxSpawn = 5;
    public static int enemy;
    void Start()
    {

        enemy = 0;
        InvokeRepeating("crear", 0.0f, timepodeSpawn);
      

    }

    // Update is called once per frame
    void Update()
    {   
        
        if (enemy > 4) {

            CancelInvoke("crear");
        }
       
    }

    public void crear()
    {
       
        Vector3 SpawnPosition = new Vector3(0, 0, 0);
        SpawnPosition = this.transform.position + Random.onUnitSphere * Rangocreacion;
        SpawnPosition = new Vector3(SpawnPosition.x,
                                    SpawnPosition.y,
                                    SpawnPosition.z);

        GameObject cubo = Instantiate(cubos, SpawnPosition, Quaternion.identity);
        ++enemy;
    }
}
